#ifndef IOP_IRX_IMPORTS_H
#define IOP_IRX_IMPORTS_H

#include <irx.h>

/* Please keep these in alphabetical order!  */
#include <atad.h>
#include <dev9.h>
#include <intrman.h>
#include <iomanX.h>
#include <loadcore.h>
#include <ps2ip.h>
#include <sifcmd.h>
#include <sifman.h>
#include <stdio.h>
#include <sysclib.h>
#include <thbase.h>
#include <thsemap.h>
#include <thevent.h>

#endif /* IOP_IRX_IMPORTS_H */
